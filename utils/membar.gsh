#!/bin/sh


physical_free=$( free | grep Mem: | awk '{print ($5+$4)/$2*100}' | cut -f 1 -d '.' )
physical_used=$( free | grep Mem: | awk '{print (1-(($5+$4)/$2))*100}' | cut -f 1 -d '.' )
swap_free=$( free | grep Swap: | awk '{print ($4)/$2*100}' | cut -f 1 -d '.' )
swap_used=$( free | grep Swap: | awk '{print (1-($4/$2))*100}' | cut -f 1 -d '.' )

showmemory()
{
  echo -n pu: $physical_used
  _:ginger_simple_bar $physical_used
  echo -n pf: $physical_free
  _:ginger_simple_bar $physical_free
  echo -n su: $swap_used
  _:ginger_simple_bar $swap_used
  echo -n sf: $swap_free
  _:ginger_simple_bar $swap_free

exit 0
