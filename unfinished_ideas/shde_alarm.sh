        while true
        do
        HOUR=$(date | cut -d ' '  -f 4 | cut -d: -f1)
        MINUTE=$(date | cut -d ' '  -f 4 | cut -d: -f2)

                if [[ $HOUR -eq $1 ]]
                then
                        if [[ $MINUTE -eq $2 ]]
                        then
                                echo -e 'CONSOLE ALARM' | wall -n
                                exit 1
                        fi
                fi

                sleep 20
        done
