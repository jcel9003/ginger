#!/bin/bash
WHEREAMI=$(readlink -f $0)
WHEREAMI=$(echo $WHEREAMI | awk -F'/' '{for (i=2; i<=(NF-1); i++) printf("/%s", $i)}')
#VERSION=$(cat $WHEREAMI | grep GINGER_VERSION | cut -d = -f 2)

#echo $WHEREAMI
source $WHEREAMI/essentials/ginger_farsight.gig
source $WHEREAMI/essentials/ginger_tag.gig

echo '#  thank you for using ginger!                             #'
echo '#  some basic help:                                        #'
echo '#    1. everything is stored in ~/.ginger/                 #'
echo '#    2. there are helpers available! (not yet implementer) #'
echo '#       -> [helpme list ] to list all commands             #'
echo '#       -> [helpme (command)] to see description           #'
echo '#       -> [type (command)] to see source!                 #'
echo '#       -> [ginger_manual] to see less-like introduction   #'
echo '#    3. ginger system is using independent, written in     #'
echo '#       pure bash, key-value store, as an less invasive    #'
echo '#       alternative to bash env values. Bare in mind that  #'
echo '#       this k-v system is as safe as your user, meaning   #'
echo '#       so anybody with access to your namespace file      #'
echo '#       can read your values. Dont put there important     #'
echo '#       things, at least before sucurity update :)         #'
echo '#     4. be aware that ~/.gigner/ directoru is volatile    #'
echo '#       it means that it can be removed by next install    #'
echo '#       or update. Dont keep important things there!       #'
echo '#     5. if there is any funcitonality that you want and   #'
echo '#      is not yet implemented, please post and issue at    #'
echo '#      our bitbucket repo, or write it yourself and ask    #'
echo '#      make pull request! We are waiting for your commits! #'
echo '#                                                          #'
echo '# ... also we are supporting                               #'
echo '# glorious master debian linux race:)                      #'
echo '# ... fuck ubuntu! For many various reasons!               #'
echo '#                                                          #'
echo '# propably i dont need to tell this, but this project      #'
echo '# is fully open source. We are waiting for your commits!   #'
echo '# Of course this is early lapha an it is provided as is    #'
echo '# without warranty of any kind ...                         #'
echo '#                                                          #'
echo '# ...but we promise to develop ginger, fix all possible    #'
echo '# bugs and implement as much features as it is possible    #'
echo '#                                                          #'
echo '# best regards,                                            #'
echo '# esavier, deathware team                                  #'

read -N 1

_:gt_notify_n 'trying to connect to network!'
_:gt_notify_n 'using google dns!'
if ! ping 8.8.8.8 -c 1 -W 2 1>/dev/null
then
        _:gt_error_n 'no internet connection detected or google dns are down,'
        _:gt_error_N 'we need to end script here!'
fi
_:gt_status_n 'connection successfull :)'
_:gt_status_n 'we will use it in later versions to set auto autoudpates later!'
rm -rf $HOME/.ginger/

mkdir -p $HOME/.ginger
mkdir -p $HOME/.ginger/config
mkdir -p $HOME/.ginger/core
mkdir -p $HOME/.ginger/essentials
mkdir -p $HOME/.ginger/helpers
mkdir -p $HOME/.ginger/networking
mkdir -p $HOME/.ginger/other
mkdir -p $HOME/.ginger/stats
mkdir -p $HOME/.ginger/utils
mkdir -p $HOME/.ginger/hardware
mkdir -p $HOME/.ginger/processes


 cp -r $WHEREAMI/config         $HOME/.ginger/ 2>/dev/null
 cp -r $WHEREAMI/core           $HOME/.ginger/ 2>/dev/null
 cp -r $WHEREAMI/essentials     $HOME/.ginger/ 2>/dev/null
 cp -r $WHEREAMI/helpers        $HOME/.ginger/ 2>/dev/null
 cp -r $WHEREAMI/networking     $HOME/.ginger/ 2>/dev/null
 cp -r $WHEREAMI/other          $HOME/.ginger/ 2>/dev/null
 cp -r $WHEREAMI/stats     	$HOME/.ginger/ 2>/dev/null
 cp -r $WHEREAMI/utils          $HOME/.ginger/ 2>/dev/null
 cp -r $WHEREAMI/hardware       $HOME/.ginger/ 2>/dev/null
 cp -r $WHEREAMI/processes      $HOME/.ginger/ 2>/dev/null
 cp  $WHEREAMI/init $HOME/.ginger/init

if [ -f $HOME/.bashrc ]; then
        if ! grep 'source $HOME/.ginger/init' $HOME/.bashrc 1>/dev/null ; then echo 'source $HOME/.ginger/init' >> $HOME/.bashrc ; fi
fi

if [ -f $HOME/.profile ]; then
        if ! grep 'source $HOME/.ginger/init' $HOME/.profile 1>/dev/null ; then echo 'source $HOME/.ginger/init' >> $HOME/.profile ; fi
fi

_:gt_notify_n ' ginger is ready to use, after next console spawn, it will automaticaly'
_:gt_notify_n ' use configs and functions available. You shouldnt touch files in      '
_:gt_notify_n ' ~/.ginger directory. Please, remember to commit new stuff if you found'
_:gt_notify_n ' something usefull. We will try to merge it even if file format is not '
_:gt_notify_n ' fully compatible with our project (and adjust it eventually)            '
_:gt_notify_n ' Best Regards from DEATHWARE team!'
_:gt_notify_n ' Have fun!'
_:gcreset
